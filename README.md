# react-flip-box

React Flip Box

# Install:
* yarn add react-flip-box
* npm i react-flip-box

# Usage:
```
import * as Ripple from "react-flip-box"
...
const handleFlip = (flipped: boolean) => {
    console.log("flipped = ", flipped)
}
...
<FlipBox 
    first={<p>First Screen</p>}
    second={<p>Second Screen</p>}
    time={500}
    onChange={handleFlip}
/>
```